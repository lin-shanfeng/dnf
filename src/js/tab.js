class Tab{
    constructor(classname){
        this.container = document.querySelector('.'+classname)
        this.ulis = document.querySelectorAll('.navBox_1>ul>li')
        this.olis = document.querySelectorAll('.tab>ol>li')
    }
    init(){
        // 绑定事件
        for(var i=0;i<this.ulis.length;i++){
            this.ulis[i].onclick = this.click(i)
            console.log(this.ulis[i])
        }
    }
    click(j){
        return ()=>{
            for(var i=0;i<this.ulis.length;i++){
                this.ulis[i].className = this.olis[i].className = '';
            }
            this.ulis[j].className = this.olis[j].className = 'active';
        }
    }
}