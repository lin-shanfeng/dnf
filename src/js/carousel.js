class Carousel{
    constructor(classname){
        this.container = document.querySelector('.'+classname)
        this.ul = this.container.querySelector('ul')
        this.bann_sides = document.querySelector('.bann-sides')
        this.ol = document.querySelector('ol')
        // this.imges = document.querySelector('.bann-item-img')
        // this.textz = document.querySelector('.bann-item-text')
        this.firstLi = this.ul.children[0]
        this.index = 1;
        this.flag = true
        this.timerId = null
    }
    init(){
        // 处理ul
        this.handlerUl()
        this.auto()
        this.dotClick()
        this.container.onmouseover = ()=>clearInterval(this.timerId)
        this.container.onmouseout = ()=>this.auto()
    }
    auto(){
        this.timerId = setInterval(()=>{
            if(!this.flag){
                return false;
            }
            this.flag = false
            this.index++
            this.move()
        },1000)
    }
    dotClick(){
        for(let i=0;i<this.ol.children.length;i++){
            this.ol.children[i].onclick = ()=>{
                if(!this.flag){
                    return false;
                }
                this.flag = false
                this.index = i+1;
                this.move()
            }
        }
    }
    move(){
        tool.animate(this.ul,{
            left:-this.index * this.firstLi.offsetWidth
        },()=>{
            if(this.index === this.ul.children.length-1){
                this.index = 1
                this.ul.style.left = -this.index * this.firstLi.offsetWidth + "px"
            }
            if(this.index === 0){
                this.index = this.ul.children.length-2
                this.ul.style.left = -this.index * this.firstLi.offsetWidth + "px"
            }
            for(var i=0;i<this.ol.children.length;i++){
                this.ol.children[i].classList.remove('swiper-slide-thumb-active');
            }
             this.ol.children[this.index-1].classList.add('swiper-slide-thumb-active');
            this.flag = true
        })
    }
    handlerUl(){
        // 前后各复制一个li
        var firstLi = this.firstLi.cloneNode(true);
        var lastLi = this.ul.lastElementChild.cloneNode(true);
        this.ul.appendChild(firstLi)
        this.ul.insertBefore(lastLi,this.firstLi)
        // 设置宽度和left
        this.ul.style.width = this.firstLi.offsetWidth * this.ul.children.length + "px";
        this.ul.style.left = -this.firstLi.offsetWidth + "px";
    }
    
}